#pragma once
#include "BaseObject.h"
class EnemyCannon : BaseObject
{
public:
	EnemyCannon();
	EnemyCannon(int _id, int _type, int _x, int _y, int _Width, int _Height);
	void Update(Box RectCamera, Box ninja, int Deltatime);
	bool CheckCollision(Box ninja);
	void Draw();
	~EnemyCannon();
};