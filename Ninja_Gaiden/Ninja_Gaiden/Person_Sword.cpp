#include "Person_Sword.h"



Person_Sword::Person_Sword()
{
}

Person_Sword::Person_Sword(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_human.png", 4, 1, 100);
	this->Sprite->SetAnimation(0, 2);
}

void Person_Sword::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void Person_Sword::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
	this->x += 1;
}

bool Person_Sword::CheckCollision(Box ninja)
{

	return false;
}


Person_Sword::~Person_Sword()
{
}
