#pragma once
#include "BaseObject.h"
class EnemyGhost1 : BaseObject
{
public:
	EnemyGhost1();
	EnemyGhost1(int _id, int _type, int _x, int _y, int _Width, int _Height);
	void Update(Box RectCamera, Box ninja, int Deltatime);
	bool CheckCollision(Box ninja);
	void Draw();
	~EnemyGhost1();
};