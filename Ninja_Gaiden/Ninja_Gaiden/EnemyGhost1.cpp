#include "EnemyGhost1.h"



EnemyGhost1::EnemyGhost1()
{
}

EnemyGhost1::EnemyGhost1(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_ghost.png", 3, 1, 180);
	this->Sprite->SetAnimation(0, 2);
}

void EnemyGhost1::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyGhost1::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyGhost1::CheckCollision(Box ninja)
{
	return false;
}


EnemyGhost1::~EnemyGhost1()
{
}
