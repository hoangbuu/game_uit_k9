#include "EnemyCat.h"



EnemyCat::EnemyCat()
{
}

EnemyCat::EnemyCat(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_cat.png", 2, 1, 150);
	this->Sprite->SetAnimation(0, 1);
}

void EnemyCat::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyCat::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyCat::CheckCollision(Box ninja)
{
	return false;
}


EnemyCat::~EnemyCat()
{
}
