#include "Ninja.h"



Ninja::Ninja()
{
}

Ninja::Ninja(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/Ninja/ninja.png", 8, 3, 500);
	this->Sprite->SetAnimation(0, 2);
}

Ninja::Ninja(int _id, int _type, int _x, int _y, int _Width, int _Height, int deltaTime, int aniStart, int aniEnd)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/Ninja/ninja.png", 8, 3, deltaTime);
	this->Sprite->SetAnimation(aniStart, aniEnd);
}

void Ninja::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void Ninja::Update(Box RectCamera, Box simon, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool Ninja::CheckCollision(Box simon)
{

	return false;
}


Ninja::~Ninja()
{
}
