#pragma once
#include "BaseObject.h"
class Ninja : BaseObject
{
public:
	Ninja();
	Ninja(int _id, int _type, int _x, int _y, int _Width, int _Height);
	Ninja(int _id, int _type, int _x, int _y, int _Width, int _Height, int deltaTime, int aniStart, int aniEnd);
	void Update(Box RectCamera, Box simon, int Deltatime);
	bool CheckCollision(Box simon);
	void Draw();
	~Ninja();
};