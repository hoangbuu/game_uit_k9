#include "EnemyRunningMan.h"



EnemyRunningMan::EnemyRunningMan()
{
}

EnemyRunningMan::EnemyRunningMan(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_running_man.png", 2, 1, 200);
	this->Sprite->SetAnimation(0, 1);
}

void EnemyRunningMan::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyRunningMan::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyRunningMan::CheckCollision(Box ninja)
{
	return false;
}


EnemyRunningMan::~EnemyRunningMan()
{
}
