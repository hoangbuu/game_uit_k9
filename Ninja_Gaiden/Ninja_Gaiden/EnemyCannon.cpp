#include "EnemyCannon.h"



EnemyCannon::EnemyCannon()
{
}

EnemyCannon::EnemyCannon(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_cannon.png", 2, 1, 500);
	this->Sprite->SetAnimation(0, 1);
}

void EnemyCannon::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyCannon::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyCannon::CheckCollision(Box ninja)
{
	return false;
}


EnemyCannon::~EnemyCannon()
{
}
