#include "EnemyHulk.h"



EnemyHulk::EnemyHulk()
{
}

EnemyHulk::EnemyHulk(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_hulk.png", 4, 1, 150);
	this->Sprite->SetAnimation(0, 3);
}

void EnemyHulk::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyHulk::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyHulk::CheckCollision(Box ninja)
{
	return false;
}


EnemyHulk::~EnemyHulk()
{
}
