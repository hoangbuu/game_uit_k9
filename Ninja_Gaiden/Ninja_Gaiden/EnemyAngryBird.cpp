#include "EnemyAngryBird.h"



EnemyAngryBird::EnemyAngryBird()
{
}

EnemyAngryBird::EnemyAngryBird(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_angry_bird.png", 2, 1, 120);
	this->Sprite->SetAnimation(0, 1);
}

void EnemyAngryBird::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyAngryBird::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyAngryBird::CheckCollision(Box ninja)
{
	return false;
}


EnemyAngryBird::~EnemyAngryBird()
{
}
