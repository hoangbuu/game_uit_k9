#ifndef _OBJMANAGER_H_
#define _OBJMANAGER_H_

#include "QuadTree.h"
#include "BaseObject.h"
#include <vector>
#include <d3d9.h>
#include <list>
#include <string>
#include <fstream>
#include <iostream>
#include "Font.h"
#include <stdio.h>
#include <d3d9.h>
#include "Collision.h"
#include "Person_Sword.h"
#include "EnemyGhost1.h"
#include "EnemyCat.h"
#include "Ninja.h"
#include "EnemyAngryBird.h"
#include "EnemyButterfly.h"
#include "EnemyEagle.h"
#include "EnemyHulk.h"
#include "EnemyHuman.h"
#include "EnemyRunningMan.h"
#include "EnemyCannon.h"
#include "Sword.h"
#include "Boss.h"
#include "Ninja.h"

using namespace std;


class ObjManager
{
private:
	vector<int*> InfoObj; //
	vector<int*> InfoObjTemp;
	QuadTree* quadTree;
	list<int> listID;
	vector<BaseObject*> listObj;
	//vector<Enemy*> listEnemy;
	int idsize = 0;
	RECT rect;
	ifstream f;
	Person_Sword* person_Sword;
	Ninja* ninja;
	EnemyGhost1* enemyGhost;
	EnemyCat* enemyCat;
	EnemyAngryBird* enemyAngryBird;
	EnemyButterfly* enemyButterfly;
	EnemyEagle* enemyEagle;
	EnemyHulk* enemyHulk;
	EnemyHuman* enemyHuman;
	EnemyRunningMan* enemyRunningMan;
	EnemyCannon* enemyCannon;
	Sword* sword;
	Boss* boss;
	Ninja* ninjaJump;
	Ninja* ninjaSittingAttack;
	Ninja* ninjaStandingAttack;
	Ninja* ninjaUsingSecondWeapon;
	Ninja* ninjaJumpingAttack;
	Ninja* ninjaClimbing;
	Ninja* ninjaRunning;
public:
	bool Isclear;
	int Frozen;
	ObjManager()
	{
		quadTree = new QuadTree();
		Isclear = false;
		Frozen = 120;
	}

	bool IsNumber(char c)
	{
		if (c >= '0' && c <= '9')
			return true;
		return false;
	}

	void LoadFileInfo(char* fileGameObj)
	{

		f.open(fileGameObj, ios::in);
		string line;
		string strTemp = "";
		int i = 0;
		int *lTemp;

		while (!f.eof())
		{
			i = 0;
			lTemp = new int[7];
			strTemp = "";
			getline(f, line);
			if (line == "")
				break;
			for (int n = 0; n < line.length(); n++)
			{
				if (line[n] != '\t' && IsNumber(line[n]))
					strTemp += line[n];
				else
				{
					if (IsNumber(strTemp[0]))
					{
						int gt = atoi(strTemp.c_str());
						lTemp[i] = gt;
						strTemp = "";
						i++;
					}
				}
				if (n == line.length() - 1)
					lTemp[i] = atoi(strTemp.c_str());
			}
			lTemp[6] = 0;
			//add info object	
			InfoObj.push_back(lTemp);
		}
	}

	void Init(char* fileGameObj, char* fileQuadTree)
	{
		/*LoadFileInfo(fileGameObj);
		quadTree->GetQuadTreeFromFile(fileQuadTree);
		int* t;
		for (int i = 0; i < InfoObj.size(); i++)
		{
			t = new int[7];
			for (int j = 0; j < 7; j++)
			{
				t[j] = InfoObj[i][j];
			}
			InfoObjTemp.push_back(t);
		}*/
		//person_Sword = new Person_Sword(1, 100, 100, 100, 80, 80);
		enemyGhost = new EnemyGhost1(1, 100, 50, 50, 80, 80);
		enemyCat = new EnemyCat(1, 100, 50, 150, 80, 80);
		//ninja = new Ninja(1, 100, 150, 100, 80, 80);
		enemyAngryBird = new EnemyAngryBird(1, 100, 250, 50, 80, 80);
		enemyButterfly = new EnemyButterfly(1, 100, 250, 150, 80, 80);
		enemyEagle = new EnemyEagle(1, 100, 50, 400, 80, 80);
		enemyHulk = new EnemyHulk(1, 100, 50, 300, 80, 80);
		enemyHuman = new EnemyHuman(1, 100, 150, 300, 80, 80);
		enemyRunningMan = new EnemyRunningMan(1, 100, 150, 150, 80, 80);
		enemyCannon = new EnemyCannon(1, 100, 150, 50, 80, 80);
		sword = new Sword(1, 100, 100, 100, 80, 80);
		boss = new Boss(1, 100, 150, 400, 80, 80);
		ninjaRunning = new Ninja(1, 100, 350, 50, 80, 80, 50, 16, 18);
		ninjaJump = new Ninja(1, 100, 450, 50, 80, 80, 50, 12, 15);
		ninjaSittingAttack = new Ninja(1, 100, 350, 150, 80, 80, 100, 8, 11);
		ninjaStandingAttack = new Ninja(1, 100, 350, 250, 80, 80, 100, 0, 3);
		ninjaUsingSecondWeapon = new Ninja(1, 100, 450, 150, 80, 80, 100, 19, 21);
		ninjaJumpingAttack = new Ninja(1, 100, 450, 250, 80, 80, 50, 4, 7);
		ninjaClimbing = new Ninja(1, 100, 250, 250, 80, 80, 150, 22, 23);
	}

	void GetListIDFromquadTree(RECT rectCamera)
	{
		rect = rectCamera;
		listID.clear();
		quadTree->GetlistObj(quadTree->Root, rectCamera, listID);
		idsize = listID.size();

	}

	void AddListAnemy(vector<int> tempEnemy, int* t)
	{
		for (int i = 0; i < tempEnemy.size(); i++)
		{
			/*	Type _type;
				t = InfoObj[tempEnemy[i]];
				_type = (Type)t[1];
				Enemy* obj;
				switch (_type)
				{
				case GroundMovingPlatform:
					obj = new MovePlatform(t[0], t[1], t[2], t[3], t[4], t[5]);
					break;*/

					/*	default:
							break;
						}

						listEnemy.push_back(obj);*/
		}
	}

	void GetListObjFromQuadTree()
	{
		//get list obj game
		//bien phu
		vector<int> tempEnemy;
		std::list<int>::iterator it;
		int *t = new int[7];
		int j;
		Type _type;
		for (int i = 0; i < listObj.size(); i++)
		{
			delete listObj[i];
		}
		listObj.clear();
		for (it = listID.begin(); it != listID.end(); it++)
		{
			t = InfoObj[*it];
			RECT check{ t[2],t[3], t[2] + t[4],t[3] + t[5] };
			if (Collision::CheckCollison(rect, check))
			{
				if (t[1] >= 600 && t[1] != 615 && t[1] != 611)
				{
					BaseObject* obj = new BaseObject(t[0], t[1], t[2], t[3], t[4], t[5]);
					listObj.push_back(obj);
				}
				else
				{
					//Load id enemy
					tempEnemy.push_back(*it);
				}
			}
		}

		//loai bo pt trung
		if (!tempEnemy.empty())
			for (int i = 0; i < tempEnemy.size() - 1; i++)
			{
				for (int j = i + 1; j < tempEnemy.size(); j++)
				{
					if (tempEnemy[i] == tempEnemy[j])
					{
						tempEnemy.erase(tempEnemy.begin() + j);
						j--;
					}

				}
				//if (!tempEnemy.empty())

			}



	}

	void UpDate(RECT rectCamera, vector<Object> &listItem, int DeltaTime, Box ninja)
	{
		GetListIDFromquadTree(rectCamera);
		GetListObjFromQuadTree();
		if (Frozen < 120)
		{
			Isclear = true;
			Frozen++;
		}
		if (Isclear)
		{

		}
		this->person_Sword->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninja->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
	}
	void UpDateTest(RECT rectCamera, int DeltaTime, Box ninja)
	{
		//this->person_Sword->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyGhost->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyCat->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyAngryBird->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyButterfly->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyEagle->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyHulk->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyHuman->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyRunningMan->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->enemyCannon->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->sword->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->boss->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninjaRunning->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninjaJump->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninjaSittingAttack->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninjaStandingAttack->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninjaUsingSecondWeapon->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninjaJumpingAttack->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
		this->ninjaClimbing->Update(Box::ConvertRECT(rect), ninja, DeltaTime);
	}

	void Draw()
	{
		//this->person_Sword->Draw();
		this->enemyGhost->Draw();
		this->enemyCat->Draw();
		this->enemyAngryBird->Draw();
		this->enemyButterfly->Draw();
		this->enemyEagle->Draw();
		this->enemyHulk->Draw();
		this->enemyHuman->Draw();
		this->enemyRunningMan->Draw();
		this->enemyCannon->Draw();
		this->sword->Draw();
		this->boss->Draw();
		this->ninjaRunning->Draw();
		this->ninjaJump->Draw();
		this->ninjaSittingAttack->Draw();
		this->ninjaStandingAttack->Draw();
		this->ninjaUsingSecondWeapon->Draw();
		this->ninjaJumpingAttack->Draw();
		this->ninjaClimbing->Draw();
	}

	vector<BaseObject*> GetListObj()
	{
		return listObj;
	}

	vector<int*> GetListInfo();

	void HandlingCollisionWithGround(vector<Object> &listItem, int DeltaTime)
	{

	}
	void Reset()
	{
		InfoObj.clear();
		int* t;
		for (int i = 0; i < InfoObjTemp.size(); i++)
		{
			t = new int[7];
			for (int j = 0; j < 7; j++)
			{
				t[j] = InfoObjTemp[i][j];
			}
			InfoObj.push_back(t);
		}
	}
	void Delete()
	{
		/*for (int i = 0; i < listEnemy.size(); i++)
		{
			listEnemy[i]->IsDie = true;
			Effect::GetStaticObj()->Add(listEnemy[i]);
		}*/
	}
	~ObjManager()
	{
		//if (quadTree != NULL) delete quadTree; 
		f.close();
	}
};
#endif // !_OBJMANAGER_H_




