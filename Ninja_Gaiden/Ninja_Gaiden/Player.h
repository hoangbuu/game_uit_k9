﻿#define _PLAYER_H_
#include <d3d9.h>
#include <d3dx9.h>
#include "GSprite.h"
#include "Ninja.h"


#define TURN_LEFT -1
#define TURN_RIGHT 1 // xác định hướng và thu nhỏ phóng to
#define GROUND_Y 400 // mat dat tam thoi

enum State { STAND, RUN, JUMP, SIT };

class Player
{
private:
	GSprite * _sprite;
	int x;
	int y;
	int turn;
	int vecY;
	int VecX;
	int Santo;
	State state;
	Ninja* ninjaStanding;
public:
	Player(GSprite *sprite);
	void Update();
	void Turn(int turn);
	void Draw();
	void SetState(State _state);
	int GetY();
	int GetX();
	~Player();
};

