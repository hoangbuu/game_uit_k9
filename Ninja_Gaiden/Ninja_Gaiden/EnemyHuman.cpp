#include "EnemyHuman.h"



EnemyHuman::EnemyHuman()
{
}

EnemyHuman::EnemyHuman(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_human.png", 3, 1, 250);
	this->Sprite->SetAnimation(0, 2);
}

void EnemyHuman::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyHuman::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyHuman::CheckCollision(Box ninja)
{
	return false;
}


EnemyHuman::~EnemyHuman()
{
}
