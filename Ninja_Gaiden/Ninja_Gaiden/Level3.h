#pragma once
#include "Scene.h"
#include <Windows.h>
#include <cstdlib> 
#include <ctime> 
#include <dinput.h>
#include "Scene.h"
#include <d3d9.h>
#include <dinput.h>
#include "Global.h"
#include "GTexture.h"
#include "GSprite.h"
#include "GCamera.h"
#include "Map.h"
#include "Global.h"
#include "ObjManager.h"
#include "KeyBoard.h"
#include "Font.h"

class Level3 : public Scene
{
	GCamera* camera;
	Map* map;
	//Simon* simon;
	ObjManager* objManager;
	//BlackBoard* blackBoard;
public:
	Level3();
	virtual void RenderFrame(int Delta);
	virtual void ProcessInput_UP(int Delta);
	virtual void ProcessInput_DOWN(int Delta);
	virtual void ProcessInput_RIGHT(int Delta);
	virtual void ProcessInput_LEFT(int Delta);
	virtual void ProcessInput(int Delta);
	virtual void UpdateGame(int Delta);
	virtual void LoadResources();
	virtual void OnKeyDown(int KeyCode);
	~Level3();
};