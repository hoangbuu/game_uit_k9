#include "EnemyEagle.h"



EnemyEagle::EnemyEagle()
{
}

EnemyEagle::EnemyEagle(int _id, int _type, int _x, int _y, int _Width, int _Height)
{
	index = 0;
	temp = 0;
	x = _x;
	y = _y;
	Type = _type;
	id = _id;
	vecX = 4;
	vecY = 0;
	LoadResource(L"Resources/enemy/enemy_eagle.png", 2, 1, 70);
	this->Sprite->SetAnimation(0, 1);
}

void EnemyEagle::Draw()
{
	//Sprite->SelectIndex(index);
	Sprite->Draw(x, y);
}

void EnemyEagle::Update(Box RectCamera, Box ninja, int Deltatime)
{
	this->Sprite->Update(Deltatime);
}

bool EnemyEagle::CheckCollision(Box ninja)
{
	return false;
}


EnemyEagle::~EnemyEagle()
{
}
